# amalgameClassifier

*Pauline Lefebvre, Michael Nauge, Laboratoire FoReLLIS, Université de Poitiers*


Ensemble de fonctions python permettant la classification des néologismes de type amalgame.